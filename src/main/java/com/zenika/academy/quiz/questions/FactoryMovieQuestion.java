package com.zenika.academy.quiz.questions;

import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class FactoryMovieQuestion {

    //Le singleton
    private static FactoryMovieQuestion single;

    static {
        try {
            single = new FactoryMovieQuestion(new File("src/main/resources/tmdb-api-key.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Variable d'instance
    public TmdbClient tmdbClient;
    public Random random;

    //Constructeur privé
    private FactoryMovieQuestion(File fileApiKey) throws IOException {
        TmdbClient client = new TmdbClient(fileApiKey);
        this.tmdbClient = client;
        this.random = new Random();
    }

    FactoryMovieQuestion(int seed, TmdbClient tmdbClient) {
        this.tmdbClient = tmdbClient;
        random = new Random(seed);
    }

    //Méthode d'accès au singleton
    public static FactoryMovieQuestion getInstance() throws IOException {
        if (single == null)
            single = new FactoryMovieQuestion(new File("src/main/resources/tmdb-api-key.txt"));
        return single;
    }

    //Accesseur
    public TmdbClient getClient() {
        return this.tmdbClient;
    }

    public Optional<Question> generateMovieFormTitle(String title) {
        Optional<MovieInfo> movie = tmdbClient.getMovie(title);
        return movie.map((MovieInfo movieInfo) -> getQuestionMovie(movieInfo));
    }

    public Optional<Question> generateActorFromTitle(String title) {
        Optional<MovieInfo> movieTwo = tmdbClient.getMovie(title);
        return movieTwo.map(this::getQuestionActor);
    }

    public Optional<Question> generatePersonnageFromTitle(String title) {
        Optional<MovieInfo> movieThree = tmdbClient.getMovie(title);
        return movieThree.map(this::getQuestionPersonnage);
    }

    public Optional<Question> generateVFQuestion(String title) {
        Optional<MovieInfo> movieTF = tmdbClient.getMovie(title);
        return movieTF.map((MovieInfo movieInfo) -> getTrueFalseMovieQuestion(movieInfo));
    }

    public Optional<Question> generateMultipleChoicesQuestion (String title) {
        Optional<MovieInfo> movieTF = tmdbClient.getMovie(title);
        return movieTF.map((MovieInfo movieInfo) -> getMultipleChoiceMovieQuestion (movieInfo));
    }

    public Question getQuestionMovie(MovieInfo movieInfo) {
        return new OpenQuestion("Quel film sorti en " + movieInfo.year + " contient des personnages joués par l'acteur "
                + movieInfo.cast.get(1).actorName + " ?", movieInfo.title);
    }

    public Question getQuestionActor(MovieInfo movieInfo) {
        return new OpenQuestion("Quel acteur joue le personnage de " + movieInfo.cast.get(0).characterName + " dans le film "
                + movieInfo.title + " ?", movieInfo.cast.get(0).actorName);
    }

    public Question getQuestionPersonnage(MovieInfo movieInfo) {
        return new OpenQuestion("Quel est le personnage interprété par " + movieInfo.cast.get(1).actorName + " dans le film "
                + movieInfo.title + " ?", movieInfo.cast.get(1).characterName);
    }

    public Question getTrueFalseMovieQuestion(MovieInfo movieInfo) {
        return new TrueFalseQuestion(movieInfo.cast.get(0).actorName +
                " a-t'il joué dans " + movieInfo.title + " ?", true);
    }

    public Question getMultipleChoiceMovieQuestion(MovieInfo movieInfo) {
        return new MultipleChoiceQuestion("Dans quel film a joué " + movieInfo.cast.get(0).actorName + " ? ",
                List.of(tmdbClient.getRandomPopularMovie().get().title,
                        tmdbClient.getRandomPopularMovie().get().title,
                        tmdbClient.getRandomPopularMovie().get().title),
                movieInfo.title,
                random);
    }
}
