package com.zenika.academy.quiz.questions;

import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Year;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FactoryMovieQuestionTest {

    @Test
    void generateMovieFormTitleTest() throws IOException {
        FactoryMovieQuestion factoryMovieQuestion = FactoryMovieQuestion.getInstance();
        assertEquals("Quel film sorti en 1997 contient des personnages joués par l'acteur Leonardo DiCaprio ?",
                factoryMovieQuestion.generateMovieFormTitle("Titanic").get().getDisplayableText());
    }

    @Test
    void generateActorFromTitleTest() throws IOException {
        FactoryMovieQuestion factoryMovieQuestion = FactoryMovieQuestion.getInstance();
        assertEquals("Quel acteur joue le personnage de Bill Denbrough dans le film It Chapter Two ?",
                factoryMovieQuestion.generateActorFromTitle("It").get().getDisplayableText());
    }

    @Test
    void generatePersonnageFromTitleTest() throws IOException {
        FactoryMovieQuestion factoryMovieQuestion = FactoryMovieQuestion.getInstance();
        assertEquals("Quel est le personnage interprété par Robert Pattinson dans le film Twilight ?",
                factoryMovieQuestion.generatePersonnageFromTitle("Twilight").get().getDisplayableText());
    }

    @Test
    void generatePersonnageFormTitleTestMock() {
        //arrange
        MovieInfo movieInfo = new MovieInfo("Titanic", Year.of(1997),
                List.of(new MovieInfo.Character("Jack", "Leonardo"),
                        new MovieInfo.Character("Rose", "Kate"),
                        new MovieInfo.Character("Molly", "Katy")));
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        when(mockTmdbClient.getMovie("Titanic")).thenReturn(Optional.of(movieInfo));
        FactoryMovieQuestion factoryMovieQuestion = new FactoryMovieQuestion(1,mockTmdbClient);
        //act
        Optional<Question> optionalQuestion = factoryMovieQuestion.generatePersonnageFromTitle("Titanic");
        //assert
        assertTrue(optionalQuestion.isPresent());
        assertEquals("Quel est le personnage interprété par Kate dans le film Titanic ?", optionalQuestion.get().getDisplayableText());
        assertEquals(AnswerResult.CORRECT, optionalQuestion.get().tryAnswer("Rose"));
    }
    @Test
    void generateActorFromTitleMock() {
        //arrange
        MovieInfo movieInfo = new MovieInfo("Star Wars : The Empire Strikes Back", Year.of(1977),
                List.of(new MovieInfo.Character("Luke Skywalker", "Mark Hamill"),
                        new MovieInfo.Character("Han Solo", "Harrison Ford"),
                        new MovieInfo.Character("Princesse Leia", "Carrie Fisher")));
        TmdbClient mockTmdbClient = mock(TmdbClient.class);
        when(mockTmdbClient.getMovie("Star Wars : The Empire Strikes Back")).thenReturn(Optional.of(movieInfo));
        FactoryMovieQuestion factoryMovieQuestion = new FactoryMovieQuestion(0,mockTmdbClient);
        //act
        Optional<Question> optionalQuestion = factoryMovieQuestion.generateActorFromTitle("Star Wars : The Empire Strikes Back");
        //assert
        assertTrue(optionalQuestion.isPresent());
        assertEquals("Quel acteur joue le personnage de Luke Skywalker dans le film Star Wars : The Empire Strikes Back ?", optionalQuestion.get().getDisplayableText());
        assertEquals(AnswerResult.CORRECT, optionalQuestion.get().tryAnswer("Mark Hamill"));
    }

}

